<section class="entry-content">
<?php if ( has_post_thumbnail() ) { the_post_thumbnail(); } ?>
    <p><?php the_content(); ?></p>
<div class="entry-links"><?php wp_link_pages(); ?></div>
</section>