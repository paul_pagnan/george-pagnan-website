<?php get_header(); ?>
<br>
</div>

<div class="row body-content">
    <div class="small-12 searchpage">
        <?php if ( have_posts() ) : ?>
        <h1 class="entry-title"><?php printf( __( 'Search Results for: "%s"', 'blankslate' ), get_search_query() ); ?></h1>
        <hr><br>
        <?php while ( have_posts() ) : the_post(); ?>
        <?php get_template_part( 'entry' ); ?>
        <?php endwhile; ?>
        <?php get_template_part( 'nav', 'below' ); ?>
        <?php else : ?>
        <article id="post-0" class="post no-results not-found">
            <br><br>
            <header>
                <h2 class="entry-title"><?php _e( 'Nothing Found', 'blankslate' ); ?></h2>
            </header>
            <section class="entry-content">
                <p><?php _e( 'Sorry, nothing matched your search. Please try again.', 'blankslate' ); ?></p>
                
            </section>
        </article>
        <?php endif; ?>
    </div>
</div>

<?php get_footer(); ?>