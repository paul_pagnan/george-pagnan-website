$(document).ready(function() {
    $.getJSON('<?php echo bloginfo('template_url');?>/getTweets.php', function(data) {
        var tweets = [];

        var wrap = "<div class='medium-3 columns tweet'>%</div>";
        var link = "<b><a href='https://twitter.com/%' target='_blank'>@%</a></b><hr>";
        var body = "<p>%</p>";
        var date = "<i>%</i>";


        $.each( data, function() {
            var item1 = "";
            var item2 = "";
            var item3 = "";
            $.each( this, function( key, val ) {
                switch(key) {
                    case "user":
                        item1 = link.replace("%", val);
                        item1 = item1.replace("%", val);
                        break;
                    case "text":
                        item2 = body.replace("%", val);
                        break;
                    case "time":
                        item3 = date.replace("%", val);
                        break
                        default: break;
                }
            });
            var tweet = wrap.replace("%", item1 + item2 + item3);
            tweets.push(tweet);
        });

        var tweetGroup = [];
        var j = -1;
        for(var i = 0; i < tweets.length; i++)
        {   
            if(i%4 == 0)
            {
                j++;                   
            } 

            if(tweetGroup[j] === undefined)
                tweetGroup[j] = tweets[i];                       
            else
                tweetGroup[j] += tweets[i];            
        }

        for(var i = 0; i < tweetGroup.length; i++)
        {   
            $("#feed").append("<li data-orbit-slide='headline-" + (i+1) + "'>" + tweetGroup[i] + "</li>");
        }
       
 
        $("#load").hide();
        $("#feed").fadeIn("medium");
        
    });    
});
