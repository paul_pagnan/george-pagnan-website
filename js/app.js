// Foundation JavaScript
// Documentation can be found at: http://foundation.zurb.com/docs
$(document).foundation({
    orbit: {
      animation: 'slide', // Sets the type of animation used for transitioning between slides, can also be 'fade'
      timer_speed: 13000, // Sets the amount of time in milliseconds before transitioning a slide
      pause_on_hover: false, // Pauses on the current slide while hovering
      resume_on_mouseout: false, // If pause on hover is set to true, this setting resumes playback after mousing out of slide
      next_on_click: true, // Advance to next slide on click
      animation_speed: 500, // Sets the amount of time in milliseconds the transition between slides will last
      stack_on_small: false,
      navigation_arrows: false,
      slide_number: false,
    } 
});

if(!(/Android|iPhone|iPad|iPod|BlackBerry|Windows Phone/i).test(navigator.userAgent || navigator.vendor || window.opera)){
    skrollr.init({
        forceHeight: false
    });
}


$(window).load(function() {
   stickFooter();
   if(!(/Android|iPhone|iPad|iPod|BlackBerry|Windows Phone/i).test(navigator.userAgent || navigator.vendor || window.opera))
        checkSkrollr(); 
    else
        if($(".header .hide-for-small").css("display") != "none")
            $(".contain-to-grid").hide();
        else
            $(".contain-to-grid").show();
    
    if($(".header .hide-for-small").css("display") != "none")
    {
        if($(window).scrollTop() > 50)
            $(".contain-to-grid").show();      
        else
            $(".contain-to-grid").hide();

    }
});

$(window).resize(function() {
    stickFooter();
    if(!(/Android|iPhone|iPad|iPod|BlackBerry|Windows Phone/i).test(navigator.userAgent || navigator.vendor || window.opera))
        checkSkrollr(); 
    else
        if($(".header .hide-for-small").css("display") != "none")
            $(".contain-to-grid").hide();
        else
            $(".contain-to-grid").show();
});

$(window).scroll(function() {
    
    if($(".header .hide-for-small").css("display") != "none")
    {
        if($(window).scrollTop() > 50)
            $(".contain-to-grid").show();      
        else
            $(".contain-to-grid").hide();
                        
    }
});


function checkSkrollr() {
    if($(".header .hide-for-small").css("display") == "none")
    {
        skrollr.init().destroy();
    }
    else
    {
       skrollr.init({
            forceHeight: false
       }); 
    }
}

var currentElement;

$(".header-menu .has-dropdown a").hover(function(e) {
    if($("#menudropdown")[0] !== undefined) {
        $("#menudropdown")[0].remove();
    }
    
    var element = e.target;
    currentElement = element;
    var inner = $(element).parent().find(".dropdown");
    var tag = $(inner).html();
    tag = "<div id='menudropdown'><ul class='medium f-dropdown'>" + tag + "</ul></div>";

    $("body").append(tag);

    var left = $(element).offset().left - 5;
    var top = $(element).offset().top + $(element).height() - $("#wpadminbar").height() - 5;

    $("#menudropdown").css("left", left);
    $("#menudropdown").css("top", top);

});

$("#menudropdown").mouseleave(function() {
    $("#menudropdown")[0].remove();
});

$(".header-menu li").mouseenter(function(e) {
    var element = e.target;
    
    if(element != currentElement)
        $("#menudropdown")[0].remove();
});

$(".row").hover(function(e) {
    //    console.log("a")
        var element = e.target;
//        console.log(element);
        if(element != currentElement && $(element) != $("#menudropdown"))
        {
            $("#menudropdown").remove();
        }
});


function stickFooter() {
    if($(".body-content")[0] !== undefined) {
        if(($(".body-content").height() + $(".header").height() + 200) < $(window).height())
            $(".footer").addClass("footer-stick");
        else if ((($(".body-content").height() + $(".header").height()) < $(window).height()) && $(".footer").hasClass("footer-stick"))
            $(".footer").removeClass("footer-stick");
    }
}
