<?php get_header(); ?>
</div>
<div class="row body-content">
    <div class="small-12">
        <section id="content" role="main">
            <article id="post-0" class="post not-found">

            <h1 class="entry-title">404 <?php _e( 'Not Found', 'blankslate' ); ?></h1>
                <section class="entry-content">
                    <p><?php _e( 'Nothing found for the requested page. Try a search instead?', 'blankslate' ); ?></p>
                </section>
            </article>
        </section>
    </div>
</div>
<?php get_footer(); ?>