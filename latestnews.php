<?php
/*
Template Name: latestnews
*/
?>
<?php get_header(); ?>
<br>
</div>
<div class="row body-content">
    <div class="small-12 medium-10 columns padding-right">
        <h1>Latest News</h1>
        <br>
        <?php 
    $args = array(
        'numberposts' => 10,
        'orderby' => 'post_date',
        'order' => 'DESC',
	'post_status' => array('publish')
    );
    $recent_posts = wp_get_recent_posts( $args );
    $count=0;
    foreach ($recent_posts as $post) 
    { 
        $date = date_create($post["post_date"]);
        $formatedDay = date("jS", $date->getTimestamp());
        $formatedMonth = date("M", $date->getTimestamp());
        ?>            
        <div class="row">
            <div class="medium-2 columns">
                <div class="circle-stroke">
                    <span><?php echo $formatedDay; ?><br><?php echo $formatedMonth; ?></span>
                </div>            
            </div>
            <div class="medium-10 columns">
                <a href="<?php echo get_permalink( $post["ID"] ); ?>"><h2><?php echo $post["post_title"]; ?></h2></a>
                <p>
                    <?php 
                        $cleantext = strip_tags($post["post_content"]);
                        echo substr($cleantext ,0, 250);
                        if(strlen($cleantext) > 250)
                        {
                            echo "...";
                        }
                    ?>
                    <br><br> <a href="<?php echo get_permalink( $post["ID"] ); ?>">Read more...</a></p>
            </div>
            <br> 
        </div>     
        <hr>
    <?php 
        } 
    ?>
    </div>
    <div class="small-12 medium-2 columns padding-left">
        <h2 class="archive">Archive</h2>
        <hr>
        <ul>
            <?php wp_get_archives('type=yearly'); ?>
        </ul>
    </div>
</div>
<br><br>

<?php get_footer(); ?>
