<section class="<?php if( is_singular() ) { ?> entry-meta <?php } ?>">
<span class="entry-date"><?php the_time( get_option( 'date_format' ) ); ?></span>
</section>