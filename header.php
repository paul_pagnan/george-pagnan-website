<!DOCTYPE html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title><?php bloginfo('name'); ?> | <?php wp_title(); ?> </title>
        <link rel="stylesheet" href="<?php echo bloginfo('template_url'); ?>/css/app.css" />
        <link rel="icon" type="image/png" href="<?php echo bloginfo('template_url'); ?>/images/favicon.png">
        <?php wp_head(); ?>
    </head>
    <body>
        <div class="contain-to-grid sticky" data-10p="opacity: 0;" data-30p="opacity:1;">
            <nav class="top-bar" data-topbar role="navigation">
                <ul class="title-area">
                    <li class="name">
                        <a href="/"><img src="<?php echo bloginfo('template_url');?>/images/GPLogoWhite.png" alt="George Pagnan" /></a>
                    </li>
                    <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
                </ul>

                <section class="top-bar-section">
                    <?php
                        $options = array(
                            'menu' => 'Main',
                            'theme_location' => 'primary',
                            'container' => false,
                            'depth' => 2,
                            'items_wrap' => '<ul id="%1$s" class="right %2$s">%3$s</ul>',
                            'walker' => new GC_walker_nav_menu()
                        );
                        wp_nav_menu($options); 
                    ?>
                </section>
            </nav>
        </div>
            <div class="header">
		<div class="show-for-small small-text-center">                
            		<a href="/"><img class="logo" src="<?php echo bloginfo('template_url');?>/images/GPLogoWhite.png" alt="<?php bloginfo('name'); ?>" /></a>
		</div>
                <div class="row relative hide-for-small">
                    <div class="small-12 ">
                        <div class="search">
                            <?php get_search_form(); ?>
                        </div>
                        <div class="header-menu">
                            <a href="/"><img class="logo" src="<?php echo bloginfo('template_url');?>/images/GPLogoWhite.png" alt="<?php bloginfo('name'); ?>" /></a>
                            <div class="header-right">
                                <?php
                                    $options = array(
                                        'menu' => 'Main',
                                        'theme_location' => 'primary',
                                        'container' => false,
                                        'depth' => 2,
                                        'items_wrap' => '<ul>%3$s</ul>',
                                        'walker' => new GC_walker_nav_menu()
                                    );
                                    wp_nav_menu($options); 
//                                      wp_nav_menu( array('menu' => 'Main', 'walker' => new GC_walker_nav_menu() ));
                                ?>       
                            </div>
                        </div>
                    </div>
                </div>
	
                
                
<!--
            </div>
        </div>
-->
