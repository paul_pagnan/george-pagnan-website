<?php 
    get_header(); 
    $year = get_the_time( 'Y' );
    $month = get_the_time( 'm' );
    $longMonth = get_the_time( 'F' );
    $day = get_the_time( 'd' );
    $longDay = get_the_time( 'l, jS' );
?>
<br>
</div>
<div class="row body-content">
    <div class="small-12 medium-10 columns padding-right">
        <ul class="breadcrumbs">
            <li><a href="/">Home</a></li>
            <li><a href="<?php echo get_permalink(69); ?>">News</a></li>
            <?php
                if ( is_day() || is_month() ) { echo "<li><a href='/$year'>$year</a></li>"; }
                elseif ( is_year() ) { echo "<li class='current'><a href='/$year'>$year</a></li>"; }

                if ( is_day() ) { echo "<li><a href='/$year/$month'>$longMonth</a></li>"; }
                elseif( is_month() ) { echo "<li class='current'><a href='/$year/$month'>$longMonth</a></li>"; }

                if ( is_day() ) { echo "<li class='current'><a href='/$year/$month/$day'>$longDay</a></li>"; }
            ?>
            
        </ul>
        <h1><?php 
            if ( is_day() ) { printf( __( 'Daily Archives: %s', 'blankslate' ), get_the_time( get_option( 'date_format' ) ) ); }
            elseif ( is_month() ) { printf( __( 'Monthly Archives: %s', 'blankslate' ), get_the_time( 'F Y' ) ); }
            elseif ( is_year() ) { printf( __( 'Yearly Archives: %s', 'blankslate' ), get_the_time( 'Y' ) ); }
            else { _e( 'Archives', 'blankslate' ); }
        ?></h1>
        <hr>
        <br>
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <?php get_template_part( 'entry', 'fancy' ); ?>
        <?php endwhile; endif; ?>   
    
    </div>
    <div class="small-12 medium-2 columns padding-left">
        <h2 class="archive">Archive</h2>
        <hr>
            <?php 
                if ( is_month() || is_day() ) { echo get_day_archive(get_the_time( 'm' ), get_the_time( 'Y' )); }
                elseif ( is_year() ) { echo get_month_archive(get_the_time( 'Y' )); }
                else {  wp_get_archives('type=yearly'); }
            ?>
        
    </div>
</div>
<br><br>


<?php get_footer(); ?>
