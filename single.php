<?php get_header(); ?>
<br>
</div>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<div class="row body-content">
    <div class="small-12">
        <ul class="breadcrumbs">
            <li><a href="/">Home</a></li>
            <li><a href="<?php echo get_permalink(69); ?>">News</a></li>
            <li class="current"><a href="#"><?php the_title() ?> </a></li>
        </ul>
        <section id="content" role="main">
            
            <?php get_template_part( 'entry' ); ?>
           
        </section>
        
    </div>
</div>
<?php endwhile; endif; ?>
<br><br>
<?php get_footer(); ?>
