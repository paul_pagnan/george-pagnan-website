<?php

    add_theme_support('nav-menus');
    if (function_exists('register_nav_menus')) {
        register_nav_menus( 
            array( 
                'main' => 'Main Nav'
            )
        );
    }


    class GC_walker_nav_menu extends Walker_Nav_Menu {
        // add classes to ul sub-menus
        function start_lvl(&$output, $depth) {
            // depth dependent classes
            $indent = ( $depth > 0 ? str_repeat("\t", $depth) : '' ); // code indent

            // build html
            $output .= "\n" . $indent . '<ul class="dropdown">' . "\n";
        }
    }

    if (!function_exists('GC_menu_set_dropdown')) :
    function GC_menu_set_dropdown($sorted_menu_items, $args) {
        $last_top = 0;
        foreach ($sorted_menu_items as $key => $obj) {
            // it is a top lv item?
            if (0 == $obj->menu_item_parent) {
                // set the key of the parent
                $last_top = $key;
            } else {
                $sorted_menu_items[$last_top]->classes['dropdown'] = 'has-dropdown';
            }
        }

        return $sorted_menu_items;
    }
    endif;
    add_filter('wp_nav_menu_objects', 'GC_menu_set_dropdown', 10, 2);

    add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);
    function special_nav_class($classes, $item){
        if( in_array('current-menu-item', $classes) ){
            $classes[] = 'active ';
        }
        return $classes;
    }


    function tag_wrap($tag, $content) {
        return "<" . $tag . ">" . $content . "</" . $tag . ">";
    }


    function get_day_archive($month, $year)
    {
        global $wpdb;
        $days = $wpdb->get_results("SELECT DISTINCT MONTH(post_date) AS `Month`, DAY(post_date) AS `Day` FROM wp_posts WHERE MONTH(post_date) = '$month' AND YEAR(post_date) = $year AND post_status = 'publish' AND post_type='post' ORDER BY post_date DESC");
    
        $strBuild = "<ul>";
        foreach($days as $day)
        {
            $strBuild .= "<li><a href='/$year/$month/$day->Day'>" . get_day_str($day->Day, $month, $year) . "</a></li>";
        }
        $strBuild .= "</ul>";
        return $strBuild;
    }
    
    function get_month_archive($year)
    {
        global $wpdb;
        $months = $wpdb->get_results("SELECT DISTINCT MONTHNAME(post_date) AS `Month` FROM wp_posts WHERE YEAR(post_date) = $year AND post_status = 'publish' AND post_type='post' ORDER BY post_date DESC");
        
        $strBuild = "<ul>";
        
        foreach($months as $month)
        {
            $strBuild .= "<li><a href=/$year/" . get_Month_Num($month->Month) . ">$month->Month</a></li>";
        }
        
        $strBuild .= "</ul>";
        
        return $strBuild;
    }

    function get_Month_Num($month)
    {
        $date = date_parse($month);
        $intMonth = $date['month'];
        return $intMonth;
    }

    function get_day_str($day, $month, $year)
    {
        return date("jS Y", mktime(0, 0, 0, $month, $day, $year));        
    }
   




?>
