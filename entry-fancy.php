
    <div class="row">
        <div class="medium-2 columns">
            <div class="circle-stroke">
                <span><?php the_time('jS') ?><br><?php the_time('M') ?></span>
            </div>            
        </div>
        <div class="medium-10 columns">
            <a href="<?php the_permalink(); ?>"><h2><?php the_title(); ?></h2></a>
            <p>
                <?php the_excerpt(); ?>
    
                <a href="<?php the_permalink(); ?>">Read more...</a></p>
        </div>
        <br> 
    </div>     
    <hr>

