<?php get_header(); ?>

    <div class="row relative">
        <div class="small-12 columns">
            <ul class="services-orbit" data-orbit>
                <?php
                    $args = array(
                       'sort_order' => 'ASC',
                       'sort_column' => 'ID',
                       'child_of' => 11,
                    ); 
                    $pages = get_pages($args); 
                    $count = 0;
                    foreach($pages as $page)
                    {
                        $count += 1;
                        $title = tag_wrap("h2", $page->post_title);
                        $body = tag_wrap("h3", $page->post_content) . "<a href=" . get_permalink( 67 ) . " class='button-stroke'>More info</a>";
                        $combine = tag_wrap("div", $title . $body);
                        $head = "<li data-orbit-slide='headline-" . $count . "'>";
                        echo $head . $combine;
                    }
                ?>
            </ul>
        </div>
        <div class="ftp">
            <a href="/ftp"><i class="fa fa-lock"></i> &nbsp;Secure FTP</a>
        </div>
    </div>
</div>

<div class="main">
    <div class="row">
        <div class="small-12 medium-6 columns padding-right">
            <?php 
                $page = get_page_by_title( "Index" );
                echo $page->post_content;
            ?>  
            <br>
            <a href="<?php echo get_permalink( 80 ); ?>" class="button-stroke text-black">Read More</a>
        </div>
        
        <div class="small-12 medium-6 columns padding-left">
            <br class="show-for-small">
            <hr class="show-for-small">
            <h1>Latest News</h1>
            <?php 
                $args = array(
                    'numberposts' => 3,
                    'orderby' => 'post_date',
                    'order' => 'DESC',
		    'post_status' => array('publish')
                );
                $recent_posts = wp_get_recent_posts( $args );
                $count=0;
                foreach ($recent_posts as $post) 
                { 
                    $date = date_create($post["post_date"]);
                    $formatedDay = date("jS", $date->getTimestamp());
                    $formatedMonth = date("M", $date->getTimestamp());
                    
                    $count += 1;
            ?>            
                <div class="row">
                    <div class="medium-2 columns">
                        <div class="circle-stroke">
                            <span><?php echo $formatedDay; ?><br><?php echo $formatedMonth; ?></span>
                        </div>            
                    </div>
                    <div class="medium-10 columns">
                        <a href="<?php echo get_permalink( $post["ID"] ); ?>"><h2><?php echo $post["post_title"]; ?></h2></a>
                        <p>
                            <?php 
                                $cleantext = strip_tags($post["post_content"]);
                                echo substr($cleantext ,0, 250);
                                if(strlen($cleantext) > 250)
                                {
                                    echo "...";
                                }
                            ?>
                            <br><br> <a href="<?php echo get_permalink( $post["ID"] ); ?>">Read more...</a></p>
                    </div>
                    <br> 
                </div>     
                <hr>
            <?php 

                } 
            ?>
            <a class="right text-small" href="<?php echo get_permalink( 69 ); ?>">View All Posts</a>
        </div>
    </div>
</div>


<div class="tweets">
    <div class="row">
        <div class="small-12 columns">
            <h1>Latest Tweets</h1>
            <div class="row">
                <div class="medium-1 text-center medium-left columns">
                    <div class="twitter-logo inline-block">
                        <div class="circle-stroke twitter-circle">
                            <span><i class="fa fa-2x fa-twitter"></i></span>
                        </div>
                    </div>
                </div>
                <div class="medium-11 columns">
                    <div class="inline-block tweets text-center">
                        <img id="load" src="<?php echo bloginfo('template_url');?>/images/ajax-loader.gif" />
                        <ul id="feed" class="tweets-orbit" data-orbit>
                            
                        </ul>                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "WebSite",
  "url": "http://www.georgepagnan.com.au/",
  "potentialAction": {
    "@type": "SearchAction",
    "target": "http://www.georgepagnan.com.au/?s={search_term_string}",
    "query-input": "required name=search_term_string"
  }
}
</script>
<?php get_footer(); ?>
