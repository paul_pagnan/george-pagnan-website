<div class="footer">
    <div class="row">
        <div class="small-12 columns">
            <div class="row">
                <div class="medium-4 columns text-center medium-text-left margin-bottom">
                    <i class="fa fa-phone"></i> &nbsp; (02) 4545 2710 <br>
                    <i class="fa fa-envelope"></i> &nbsp; <a href="<?php echo get_permalink(85); ?>">Email Contact</a>
                </div>
                <div class="medium-3 columns text-center margin-bottom">
                    <img class="footer-logo" src="<?php echo bloginfo('template_url');?>/images/GPLogo.png" />
                </div>
                <div class="medium-5 columns text-right small-text-center margin-bottom">
                    <img class="charted" style="width: 200px" src="<?php echo bloginfo('template_url');?>/images/CA.png" />
                    <img class="charted" style="width: 160px;"  src="<?php echo bloginfo('template_url');?>/images/RASLogo.png" />
                </div>
            </div>
            <hr>
            
            <?php
                $options = array(
                    'menu' => 'Main',
                    'theme_location' => 'primary',
                    'container' => false,
                    'depth' => 1,
                    'items_wrap' => '<ul id="%1$s" class="footer-menu">%3$s</ul>',
                );
                wp_nav_menu($options); 
            ?>
            
            <div class="right inline">
                Copyright &copy; <?php echo date("Y"); ?> - George Pagnan Chartered Accountant
            </div>
        </div>
    </div>
</div>

<script src="<?php echo bloginfo('template_url');?>/bower_components/skrollr/dist/skrollr.min.js"></script>
<script src="<?php echo bloginfo('template_url');?>/bower_components/jquery/dist/jquery.min.js"></script>
<script src="<?php echo bloginfo('template_url');?>/bower_components/foundation/js/foundation.min.js"></script>
<script src="<?php echo bloginfo('template_url');?>/js/app.js"></script>

<?php if(is_home()) { ?>
    <script type="text/javascript">
        $(document).ready(function() {
        $.getJSON('<?php echo bloginfo('template_url');?>/getTweets.php', function(data) {
            var tweets = [];

            var wrap = "<div class='medium-3 columns tweet'>%</div>";
            var link = "<b><a href='https://twitter.com/%' target='_blank'>@%</a></b><hr>";
            var body = "<p>%</p>";
            var date = "<i>%</i>";


            $.each( data, function() {
                var item1 = "";
                var item2 = "";
                var item3 = "";
                $.each( this, function( key, val ) {
                    switch(key) {
                        case "user":
                        item1 = link.replace("%", val);
                        item1 = item1.replace("%", val);
                        break;
                        case "text":
                        item2 = body.replace("%", val);
                        break;
                        case "time":
                        item3 = date.replace("%", val);
                        break
                            default: break;
                    }
                });
                var tweet = wrap.replace("%", item1 + item2 + item3);
                tweets.push(tweet);
            });

            var tweetGroup = [];
            var j = -1;
            for(var i = 0; i < tweets.length; i++)
            {   
                if(i%4 == 0)
                {
                    j++;                   
                } 

                if(tweetGroup[j] === undefined)
                    tweetGroup[j] = tweets[i];                       
                else
                    tweetGroup[j] += tweets[i];            
            }

            for(var i = 0; i < tweetGroup.length; i++)
            {   
                $("#feed").append("<li data-orbit-slide='headline-" + (i+1) + "'>" + tweetGroup[i] + "</li>");
            }

            $("#load").hide();
            $(".tweets").removeClass("text-center");
            $("#feed").fadeIn("slow");

        });    
    });
    </script>
<?php } ?>

<?php wp_footer(); ?>
</body>
</html>
